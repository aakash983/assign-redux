import React, { useEffect } from "react";
import {
  Card,
  CardContent,
  Container,
  Typography,
  CircularProgress,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchUserDetails,
  fetchUsers,
} from "../../../redux/actions/userDetails";
import { cleanup } from "@testing-library/react";

const ViewUser = () => {
  const state = useSelector((state) => state);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(fetchUserDetails(id));
    return () => {};
  }, [id]);

  const getUserDetails = useSelector((state) => state.user?.fetchUserDetails);
  return (
    <div>
      <Breadcrumbs mt={2} sx={{ marginLeft: "20px" }} aria-label="breadcrumb">
        <Link
          sx={{ cursor: "pointer" }}
          underline="hover"
          color="inherit"
          onClick={() => navigate("/", dispatch(fetchUsers()))}
        >
          Home
        </Link>
        <Typography color="text.primary">ViewUser</Typography>
      </Breadcrumbs>
      <Container sx={{ marginTop: "20px" }} maxWidth="sm">
        <Card>
          <CardContent>
            {state?.user?.isLoading ? (
              <div>
                <Typography
                  sx={{ textAlign: "left", fontWeight: "bold" }}
                  variant="h6"
                >
                  Title
                </Typography>
                <Typography sx={{ textAlign: "left" }} variant="h6">
                  {getUserDetails?.title}
                </Typography>
                <Typography
                  sx={{ textAlign: "left", fontWeight: "bold" }}
                  variant="h6"
                >
                  Body
                </Typography>
                <Typography sx={{ textAlign: "left" }} variant="h6">
                  {getUserDetails?.body}
                </Typography>
              </div>
            ) : (
              <CircularProgress />
            )}
          </CardContent>
        </Card>
      </Container>
    </div>
  );
};
export default ViewUser;
