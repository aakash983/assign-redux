import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  Card,
  Container,
  CardContent,
  Typography,
  CircularProgress,
  Grid,
  Box,
  CardActionArea,
  CardMedia,
} from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import Picture1 from "../../../assets/image/picture1.png";

const Home = () => {
  const navigate = useNavigate();
  const state = useSelector((state) => state);

  return (
    <div>
      <Box sx={{ textAlign: "center" }}>
        {state?.user?.isLoading ? (
          <Typography textAlign="center" mt={3} variant="h6">
            Data Loading...
            <CircularProgress
              color="inherit"
              size={20}
              sx={{ color: "#624cc8" }}
            />
          </Typography>
        ) : (
          ""
        )}
      </Box>

      <Container fixed>
        <Grid mt={2} container spacing={3}>
          {state?.user?.data?.length > 0 &&
            state.user.data.map((data) => (
              <Grid
                onClick={() =>
                  navigate(`/ViewUser/${data.id}`, { state: data })
                }
                item
                key={data.id}
                xs={12}
                sm={6}
                md={4}
                lg={3}
              >
                <Card>
                  <CardActionArea>
                    <CardMedia
                      component="img"
                      height="140"
                      image={Picture1}
                      alt={Picture1}
                    />
                    <CardContent>
                      <Typography
                        sx={{
                          fontSize: "15px",
                          fontWeight: "bold",
                          textAlign: "left",
                          whiteSpace: "nowrap",
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                        }}
                        variant="h5"
                      >
                        {data.title}
                      </Typography>
                      <Typography
                        sx={{ fontSize: "15px", textAlign: "left" }}
                        color="text.secondary"
                      >
                        Hosted By:{" "}
                        <Typography
                          variant="body1"
                          component="span"
                          color="#624cc8"
                        >
                          Cambridge University
                        </Typography>
                      </Typography>
                      <Typography
                        sx={{ fontSize: "15px", textAlign: "left" }}
                        color="text.secondary"
                      >
                        <LocationOnIcon sx={{ fontSize: "15px" }} /> Anekal,
                        California
                      </Typography>
                      <Typography
                        sx={{ fontSize: "15px", textAlign: "left" }}
                        color="text.secondary"
                      >
                        <AccessTimeIcon sx={{ fontSize: "15px" }} /> 2:02 PM
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
        </Grid>
      </Container>
      {state?.user?.data == null && (
        <Typography textAlign="center" mt={10} variant="h6">
          Data will show OnClick fetch api call button
        </Typography>
      )}
    </div>
  );
};
export default Home;
