import React, { useState } from "react";
import {
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Container,
  Box,
  Typography,
  Card,
  CardContent,
  Grid,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import { useForm, Controller } from "react-hook-form";
import dayjs from "dayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import DialogModal from "../../Modal/DialogModal";

const Form = () => {
  const {
    control,
    handleSubmit,
    setValue,
    watch,
    reset,
    formState: { errors },
  } = useForm();

  const today = new Date();

  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isDialogFinal, setDialogFinal] = useState(false);
  const handleOpenDialog = () => {
    setDialogOpen(true);
  };
  const handleFinalDialog = () => {
    setDialogFinal(true);
  };
  const handleCloseDialog = () => {
    setDialogOpen(false);
  };
  const handleFinalCloseDialog = () => {
    setDialogFinal(false);
    setDialogOpen(false);
    reset();
  };
  const dialogContent = (
    <Typography>Are you sure you want to submit the form?</Typography>
  );
  const schoolNameValue = watch("name");
  const handleStartDate = (date) => {
    const formattedStartDate = dayjs(date).format("YYYY-MM-DD");
    setValue("startDate", formattedStartDate);
  };
  const handleEndDate = (date) => {
    const formattedEndDate = dayjs(date).format("YYYY-MM-DD");
    setValue("endDate", formattedEndDate);
  };

  const dialogContentFinal = (
    <Typography>
      Your registration {schoolNameValue} will contact back to you!
    </Typography>
  );
  const dialogActions = (
    <div>
      <Button onClick={handleCloseDialog} color="primary">
        No
      </Button>
      <Button onClick={handleFinalDialog} color="primary">
        Yes
      </Button>
    </div>
  );
  const dialogActionsFinal = (
    <Button onClick={handleFinalCloseDialog} color="primary">
      Exit
    </Button>
  );
  const onSubmit = (data) => {
    handleCloseDialog();
    if (data) {
      handleOpenDialog();
      console.log(data);
    } else {
      console.log("Failed");
    }
  };

  return (
    <div>
      <Container maxWidth="sm" sx={{ marginTop: "10px" }}>
        <Card sx={{ minWidth: 275 }}>
          <CardContent>
            <Typography sx={{ textAlign: "center" }} variant="h5">
              Educational Form
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormControl
                fullWidth
                sx={{ marginBottom: 2, marginTop: "10px" }}
              >
                <Controller
                  name="name"
                  control={control}
                  defaultValue=""
                  rules={{ required: "School name is required" }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="School Name"
                      variant="outlined"
                      fullWidth
                      error={Boolean(errors.name)}
                      helperText={errors.name?.message}
                    />
                  )}
                />
              </FormControl>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <FormControl fullWidth sx={{ marginBottom: 2 }}>
                    <Controller
                      name="startDate"
                      control={control}
                      defaultValue=""
                      rules={{ required: "StartDate is required" }}
                      render={({ field }) => (
                        <LocalizationProvider
                          dateAdapter={AdapterDayjs}
                          locale="en"
                        >
                          <DatePicker
                            {...field}
                            onChange={(e) => handleStartDate(e)}
                            label="Start Date"
                            variant="outlined"
                            fullWidth
                          />
                        </LocalizationProvider>
                      )}
                    />
                    {errors.startDate && (
                      <Typography
                        sx={{ color: "#d32f2f", fontSize: "0.75rem" }}
                      >
                        {errors.startDate.message}
                      </Typography>
                    )}
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <FormControl fullWidth sx={{ marginBottom: 2 }}>
                    <Controller
                      name="endDate"
                      control={control}
                      defaultValue=""
                      rules={{ required: "EndDate is required" }}
                      render={({ field }) => (
                        <LocalizationProvider
                          dateAdapter={AdapterDayjs}
                          locale="en"
                        >
                          <DatePicker
                            {...field}
                            label="End Date"
                            variant="outlined"
                            onChange={(e) => handleEndDate(e)}
                            fullWidth
                          />
                        </LocalizationProvider>
                      )}
                    />
                    {errors.endDate && (
                      <Typography
                        sx={{ color: "#d32f2f", fontSize: "0.75rem" }}
                      >
                        {errors.endDate.message}
                      </Typography>
                    )}
                  </FormControl>
                </Grid>
                {/* <Grid item xs={4}>
                  <FormGroup>
                    <Controller
                      name="present"
                      control={control}
                      defaultValue={today}
                      render={({ field }) => (
                        <FormControlLabel
                          control={<Checkbox {...field} />}
                          label="Present"
                        />
                      )}
                    />
                  </FormGroup>
                </Grid> */}
              </Grid>
              <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel>Degree/Award</InputLabel>
                <Controller
                  name="degree"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Degree is required" }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      label="Degree/Award"
                      variant="outlined"
                      fullWidth
                      onChange={(e) => {
                        setValue("degree", e.target.value, {
                          shouldValidate: true,
                        });
                      }}
                    >
                      <MenuItem value="">-- Select --</MenuItem>
                      <MenuItem value="b.tech">B.Tech</MenuItem>
                      <MenuItem value="m.tech">M.Tech</MenuItem>
                      <MenuItem value="other">Other</MenuItem>
                    </Select>
                  )}
                />
                {errors.degree && (
                  <Typography sx={{ color: "#d32f2f", fontSize: "0.75rem" }}>
                    {errors.degree.message}
                  </Typography>
                )}
              </FormControl>
              <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <Controller
                  name="area"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Area is required" }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="Area of Focus/Major"
                      variant="outlined"
                      fullWidth
                      error={Boolean(errors.area)}
                      helperText={errors.area?.message}
                    />
                  )}
                />
              </FormControl>
              <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <Controller
                  name="gpa"
                  control={control}
                  defaultValue=""
                  rules={{ required: "GPA is required" }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="GPA"
                      variant="outlined"
                      fullWidth
                      error={Boolean(errors.gpa)}
                      helperText={errors.gpa?.message}
                    />
                  )}
                />
              </FormControl>

              <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel>Skills Learned</InputLabel>
                <Controller
                  name="skill"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Skill is required" }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      label="Skills Learned"
                      variant="outlined"
                      fullWidth
                      onChange={(e) => {
                        setValue("skill", e.target.value, {
                          shouldValidate: true,
                        });
                      }}
                    >
                      <MenuItem value="">-- Select --</MenuItem>
                      <MenuItem value="reactJs">ReactJS</MenuItem>
                      <MenuItem value="java">Java</MenuItem>
                      <MenuItem value="other">Other</MenuItem>
                    </Select>
                  )}
                />
                {errors.skill && (
                  <Typography sx={{ color: "#d32f2f", fontSize: "0.75rem" }}>
                    {errors.skill.message}
                  </Typography>
                )}
              </FormControl>
              <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <Controller
                  name="comments"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Comments is required" }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="Comments"
                      variant="outlined"
                      fullWidth
                      error={Boolean(errors.comments)}
                      helperText={errors.comments?.message}
                    />
                  )}
                />
              </FormControl>

              <Box sx={{ textAlign: "center" }}>
                <Button variant="contained" type="submit" color="primary">
                  Submit
                </Button>
              </Box>
            </form>
            <DialogModal
              open={isDialogOpen}
              onClose={handleCloseDialog}
              title="Save Program"
              content={dialogContent}
              actions={dialogActions}
            />

            <DialogModal
              open={isDialogFinal}
              onClose={handleFinalCloseDialog}
              title="Successfully Confirmed"
              content={dialogContentFinal}
              actions={dialogActionsFinal}
            />
          </CardContent>
        </Card>
      </Container>
    </div>
  );
};
export default Form;
