import React from "react";
import { NavLink } from "react-router-dom";
import { AppBar, Tabs, Tab, Box, Button, Toolbar } from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchUsers } from "../../redux/actions/userDetails";

const NavBar = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const disabledPaths = ["/ContactUs", "/anotherpath", "/About", "/Form", "/*"];
  const isDisabled = disabledPaths.includes(location.pathname);

  return (
    <Box sx={{ paddingTop: 10 }}>
      <AppBar
        sx={{
          bgcolor: "#624cc8",
          color: "white",
          position: "fixed",
          top: 0,
          zIndex: 1000,
        }}
      >
        <Tabs textColor="white">
          <Tab
            label="Home"
            icon={<HomeIcon />}
            iconPosition="start"
            component={NavLink}
            to="/"
            exact
            activeClassName="active"
          />
          <Tab
            label="About Us"
            component={NavLink}
            to="/About"
            activeClassName="active"
          />
          <Tab
            label="Contact"
            component={NavLink}
            to="/Form"
            activeClassName="active"
          />
          <Toolbar>
            <Button
              disabled={isDisabled}
              variant="outlined"
              color="inherit"
              size="small"
              onClick={() => dispatch(fetchUsers())}
            >
              Show Cards
            </Button>
          </Toolbar>
        </Tabs>
      </AppBar>
    </Box>
  );
};
export default NavBar;
