import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

// Action
export const fetchUsers = createAsyncThunk("fetchUsers", async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  return response.json();
});

export const fetchUserDetails = createAsyncThunk(
  "fetchUserDetails",
  async (id) => {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
    return response.json();
  }
);

const userSlice = createSlice({
  name: "user",
  initialState: {
    isLoading: false,
    data: null,
    isError: false,
    fetchUserDetails: "",
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUsers.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
    });
    builder.addCase(fetchUsers.rejected, (state, action) => {
      state.isError = true;
    });
    builder.addCase(fetchUserDetails.fulfilled, (state, action) => {
      state.fetchUserDetails = action.payload;
      state.isLoading = true;
    });
  },
});
export default userSlice.reducer;
