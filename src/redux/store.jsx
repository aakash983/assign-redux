import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./actions/userDetails";

export const store = configureStore({
  reducer: {
    user: userReducer,
  },
});
