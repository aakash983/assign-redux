import React from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import ViewUser from "./components/Pages/User/ViewUser";
import Home from "./components/Pages/Home/Home";
import NavBar from "./components/NavBar/NavBar";
import Form from "./components/Pages/Form/Form";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route
            index
            path="/"
            element={
              <div>
                <Home />
              </div>
            }
          />
          <Route path="/viewUser/:id" Component={ViewUser} />
          <Route path="/Form" Component={Form} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
